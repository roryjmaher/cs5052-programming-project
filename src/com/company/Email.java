package com.company;

import java.util.ArrayList;

/**
 * Created by rorymaher on 24/04/2016.
 * Created by David Moya Garcia 10003416
 */
public class Email implements Comparable<Email> {

    // Instance Variables
    private String emailAddress;
    private ArrayList<Word> emailWords;

    // Constructor for Email
    public Email(String emailAddress, ArrayList<Word> emailWords){
        this.emailAddress = emailAddress;
        this.emailWords = new ArrayList<>(emailWords);
    }
    // Getters
    public String getEmailAddress() {
        return emailAddress;
    }
    public ArrayList<Word> getEmailWords() {
        return emailWords;
    }

    // This toString is here for Completeness and is never used
    public String toString(){
        return "{" + this.emailAddress + " -> {" + this.emailWords + "}";
    }

    // This compareTo is here for Completeness and is never used
    @Override
    public int compareTo(Email e) {
        return this.emailAddress.compareToIgnoreCase(e.getEmailAddress());
    }
}
