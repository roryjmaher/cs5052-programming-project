package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Rory Maher 10132708
 * Created by David Moya Garcia 10003416
 */

public class EmailAnalysis {

    private TreeMap<String,ArrayList<Word>> mainAnalysis;
    private ArrayList<String> noiseWords;// ArrayList of Noise Words
    private ArrayList<String> gPost;//ArrayList of email Words (Strings)
    private ArrayList<Email> emailList;//ArrayList of Email objects

    /* In the driver We instantiate this EmailAnalysis object and pass it the file paths
    The constructor for this object calls all the necessary methods for us to successfully
    run the program
    */
    public EmailAnalysis(String emailsFile, String noiseWordsFile) {
        noiseWords = new ArrayList<>(loadFile(noiseWordsFile));
        gPost = new ArrayList<>(loadFile(emailsFile));
        emailList = new ArrayList<Email>();

        this.calculateFrequency(this.filterWords(gPost));
        this.populateTreeMap();
    }

    //  This parses the incoming files and returns them as an ArrayList of Strings
    public ArrayList<String> loadFile(String filename) {
        ArrayList<String> myList = new ArrayList<>();
        try {
            FileReader myFile = new FileReader(filename);
            FileReader testFile = new FileReader(filename);
            //Checks what type of File it is and acts accordingly
            BufferedReader myReader = new BufferedReader(myFile);
            BufferedReader typeFile = new BufferedReader(testFile);
            String lineFromFile = myReader.readLine();
            String lineTest = typeFile.readLine();
            while (lineTest.isEmpty()) {
                lineTest = typeFile.readLine();
            }
            if (lineTest.contains("gPostBegin")) {
                while (lineFromFile != null) {
                    if (lineFromFile.contains("gPostBegin") && lineFromFile.contains("gPostEnd")) {
                        myList.add(lineFromFile);
                    } else if (lineFromFile.contains("gPostBegin")) {
                        String Temp = "";
                        while (!lineFromFile.contains("gPostEnd")) {
                            Temp = Temp + lineFromFile;
                            lineFromFile = myReader.readLine();
                        }
                        Temp = Temp + lineFromFile;
                        myList.add(Temp);
                    }
                    lineFromFile = myReader.readLine();
                }
            }
            else {
                while (lineFromFile != null) {
                    myList.add(lineFromFile);
                    lineFromFile = myReader.readLine();
                }
            }
            myReader.close();
            myFile.close();
        } catch (IOException e) {
            System.out.println("Error Reading File!");
        }
        return myList;
    }


    public ArrayList<Email> filterWords(ArrayList<String> separateFilter){
        String emailVal;
        String keywordBeginning = "gPostBegin";
        String keywordEnd = "gPostEnd";
        ArrayList<String> Elements;
        ArrayList<Word> wordObjects = new ArrayList<>();
        ArrayList<Email>LocalEmail = new ArrayList<>();
        String RemoveNoise;
        int startIndex;
        int endIndex;
        int duplicateIndex;

        for(String n: gPost){
            duplicateIndex = -1;
            wordObjects.clear();

            emailVal = n.substring(0,n.indexOf(keywordBeginning));
            emailVal = emailVal.substring(0,emailVal.indexOf("@"));

            startIndex = (n.indexOf(keywordBeginning)+ keywordBeginning.length());
            endIndex = (n.indexOf(keywordEnd));

            RemoveNoise = n.substring(startIndex,endIndex);
            Elements =removeNoiseWords(RemoveNoise);

            for(String a:Elements){
                wordObjects.add(new Word(a));
            }

            // This checks if the email address is already in the ArrayList of Email objects
            // if it is get that Emails index.
            for (int x = 0; x < LocalEmail.size();x++){
                if (LocalEmail.get(x).getEmailAddress().equalsIgnoreCase(emailVal)){
                    duplicateIndex = x;
                }
            }
            // if we have a valid index we update the existing email object with the new words
            if (duplicateIndex != -1){
                LocalEmail.get(duplicateIndex).getEmailWords().addAll(wordObjects);
                wordObjects.clear();
            }
            // Otherwise we simply just add the words as a new Email Object
            else{
                LocalEmail.add(new Email(emailVal,wordObjects));
                wordObjects.clear();
            }
        }
        return LocalEmail;
    }

    private ArrayList<String> removeNoiseWords(String RemoveNoise){
        ArrayList<String> noNoise= new ArrayList<>();
            for (String n : RemoveNoise.split(" ")) {
                n = n.replaceAll("([^a-zA-Z])","");
                if(n.matches(".*\\w.*")){
                       noNoise.add(n);
                }
            }
        for(String s:noiseWords){
            for(int i=noNoise.size()-1;i>=0;i--){
                if(s.equalsIgnoreCase(noNoise.get(i)) ){
                    noNoise.remove(i);
                }
            }
        }
        return noNoise;
    }

    public void calculateFrequency(ArrayList<Email> localEmail) {

        HashSet<String> words = new HashSet<>();
        ArrayList<Word> wordList;// ArrayList of Words
        ArrayList<String> wordTextList = new ArrayList<>();
        ArrayList<Word> outputToEmail;

        int i;

        // Loops through the List of Email objects
        for (i = 0; i < localEmail.size(); i++) {
            outputToEmail = new ArrayList<>();

            wordList = new ArrayList<>(localEmail.get(i).getEmailWords());

            // Populates an ArrayList of Strings and also a HashSet of Strings concurrently
            for (int j = 0; j < wordList.size(); j++) {
                wordTextList.add(wordList.get(j).getWord().toLowerCase());// ArrayList of Strings
                words.add(wordList.get(j).getWord().toLowerCase());// HashSet of Strings
            }
            // We then iterate over the HashSet looking each Word
            // If multiple found we calculate the Frequency and
            //Add it to a new Word object
                for (String s : words) {
                    int freq = Collections.frequency(wordTextList, s);
                    outputToEmail.add(new Word(s, freq));
                }
            // We sort the Words using the Word comparable compareTo
            Collections.sort(outputToEmail);

            // This removes any Word object after the tenth index
            // We understand this is destructive, however the program will always read from
            // a file on execution so we feel it won't affect the program in general
            if (outputToEmail.size() > 10){
                outputToEmail = new ArrayList<>(outputToEmail.subList(0,10));
            }

            emailList.add(new Email(localEmail.get(i).getEmailAddress(),outputToEmail));
            wordTextList.clear();
            words.clear();
        }
    }

    public void populateTreeMap(){
        mainAnalysis = new TreeMap<>();
        for (Email e : this.emailList) {
            String emailAddress = e.getEmailAddress();
            ArrayList<Word> emailWords = e.getEmailWords();
            mainAnalysis.put(emailAddress,emailWords);
        }
        System.out.println(mainAnalysis.toString().replace("}], ","}]\n"));
    }
}