package com.company;

/**
 * Created by Rory Maher 10132708
 * Created by David Moya Garcia 10003416
 */

public class Main {

    public static void main(String[] args) {

//        File Paths for Testing
        String nWords = "/Users/rorymaher/Desktop/NoiseWords.txt";
        String emailFile = "/Users/rorymaher/Desktop/gpost.txt";

//        EmailAnalysis object which takes FilePaths
        EmailAnalysis test = new EmailAnalysis(emailFile,nWords);
    }
}
