package com.company;

/**
 * Created by Rory Maher 10132708
 * Created by David Moya Garcia 10003416
 */

public class Word implements Comparable<Word> {

    //Instance Variables
    private String word;
    private int frequency;

    //Constructor
    public Word(String word){
        this.word = word;
    }

    public Word(String word, int frequency){
        this.word = word;
        this.frequency = frequency;
    }

    // Getters & Setters
    public String getWord() {
        return word;
    }
    public void setWord(String word) {
        this.word = word;
    }
    public int getFrequency() {
        return frequency;
    }
    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    // Word toString Method
    public String toString(){
        return "{" + this.word + "," + this.frequency + "}";
    }

    // Word compareTo implements Comparable
    @Override
    public int compareTo(Word w) {
        if (this.frequency - w.frequency != 0){
            return w.frequency - this.frequency;
        }
        else{
            return this.word.compareToIgnoreCase(w.word);
        }
    }
}
